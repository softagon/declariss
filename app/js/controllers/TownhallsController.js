(function(){   

	'use strict';

	MetronicApp.controller('TownhallList', ['$rootScope', '$scope', '$location', 'Townhalls', function($rootScope, $scope, $location, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Townhalls.getAll(null, function(data){
	    	$scope.townhalls = data;
	    });

	    $scope.save = function(townhall){
	    	Townhalls.create(townhall, function(data){
	    		$location.path('/townhalls.html');
	    	})
	    }

	    $scope.modal = function(townhall) {
	    	$scope.townhallm = townhall;
	    }

	    $scope.remove = function(townhall) {
	    	Townhalls.delete(townhall.id_prefeitura, function(){
	    		var index = $scope.townhalls.indexOf(townhall);
				$scope.townhalls.splice(index, 1);
	    	},function(){
	    		townhall.rem = true;
	    	})
	    }


	}]);


	MetronicApp.controller('TownhallCreate', ['$rootScope', '$scope', '$location', 'Townhalls', 'Notaries', 'Upload', function($rootScope, $scope, $location, Townhalls, Notaries, Upload){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.townhall = {};

	    $scope.townhall.cartorios = [];

	    Notaries.getAll(null, function(data) {
			$scope.notaries = data;
		});

		$scope.cartorio = {};

		$scope.townhall.documentos = [
			{'nome': '', 'dispositivo': '', 'prazo': '' },
		];

	    $scope.addCartorio = function(data){
    		for (var i = $scope.townhall.cartorios.length - 1; i >= 0; i--) {
	    		if($scope.townhall.cartorios[i].id_cartorio == data.id_cartorio){
	    			return;
	    		}
	    	};

	    	$scope.townhall.cartorios.push(data);
	    };

	    $scope.removeCartorio = function(index){
	    	$scope.townhall.cartorios.splice(index, 1);
	    };

	    $scope.save = function(townhall, form){
	    	var logoAdministracao = $scope.townhall.logoAdministracao;
	    	var logoMunicipal = $scope.townhall.logoMunicipal;
	    	var files = [];
	    	var names = [];

	        if(logoMunicipal){
	        	files.push(logoMunicipal[0]);
	        	names.push("municipal");
	        }

	        if(logoAdministracao){
	        	files.push(logoAdministracao[0]);
	        	names.push("administracao");
	    	}

	        Upload.upload({
	            url: '../api/townhalls/'+ 0,
	            method: 'POST',
	            fileFormDataName: names,
	            file: files,
	            fields: townhall
	        }).success(function(data){
	        	$location.path('/townhalls.html');
	        });
    }

	}]);



	MetronicApp.controller('TownhallUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'Townhalls', 'Notaries', 'Upload', function($rootScope, $scope, $location, $stateParams, Townhalls, Notaries, Upload){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	   	Notaries.getAll(null, function(data) {
			$scope.notaries = data;
		});

		$scope.cartorio = {};

	    $scope.addCartorio = function(data){
    		for (var i = $scope.townhall.cartorios.length - 1; i >= 0; i--) {
	    		if($scope.townhall.cartorios[i].id_cartorio == data.id_cartorio){
	    			return;
	    		}
	    	};

	    	$scope.townhall.cartorios.push(data);
	    };

	    $scope.removeCartorio = function(index){
	    	$scope.townhall.cartorios.splice(index, 1);
	    };

	    $scope.addDocumento = function(documento){
	    	$scope.townhall.documentos.push({});
	    };

	    $scope.removeDocumento = function(index){
	    	$scope.townhall.documentos.splice(index, 1);
	    };

		Townhalls.getById($stateParams.id, function(data){
	    	$scope.townhall = data;
	    	$scope.townhall.cartorios = [];

	    	if(!$scope.townhall.documentos.length){
		    	$scope.townhall.documentos = [
					{'nome': '', 'dispositivo': '', 'prazo': '' },
				];
			}

	    	var i;
	    	for (i = 0; i < data.cartorio.length; i++) { 
			    Notaries.getById(data.cartorio[i].id_cartorio, function(cartorio) {
					$scope.townhall.cartorios.push(cartorio);
				});
	    	};
	    });

	    $scope.save = function(townhall, form){
	    	var logoAdministracao = $scope.townhall.logoAdministracao;
	    	var logoMunicipal = $scope.townhall.logoMunicipal;
	    	var files = [];
	    	var names = [];

	        if(logoMunicipal){
	        	files.push(logoMunicipal[0]);
	        	names.push("municipal");
	        }

	        if(logoAdministracao){
	        	files.push(logoAdministracao[0]);
	        	names.push("administracao");
	    	}

	        Upload.upload({
	            url: '../api/townhalls/'+ townhall.id_prefeitura,
	            method: 'POST',
	            fileFormDataName: names,
	            file: files,
	            fields: townhall
	        }).success(function(data){
	        	$location.path('/townhalls.html');
	        });
    }

	}]);



}());
