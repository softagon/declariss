(function(){

	'use strict';

	MetronicApp.controller('NotariesList', ['$rootScope', '$scope', '$location', 'Notaries', function($rootScope, $scope, $location, Notaries) {

		$scope.$on('$viewContentLoaded', function() {   
			Metronic.initAjax();
		});

		Notaries.getAll(null, function(data) {
			$scope.notaries = data;
		});

		$scope.save = function(notary){
			Notaries.create(notary, function(data) {
				$location.path('/notaries.html');
			});
		}

		$scope.modal = function(notary_modal) {
			$scope.notary_modal = notary_modal;
		}

		$scope.remove = function(notary) {
			Notaries.delete(notary.id_cartorio, function() {
				var index = $scope.notaries.indexOf(notary);
				$scope.notaries.splice(index, 1);
			}, function(){
				notary.rem = true;
			});
		}
	}]);

	MetronicApp.controller('NotaryCreate', ['$rootScope', '$scope', '$location', 'Notaries', 'NotaryUses', 'Services', 'Serventias', function($rootScope, $scope, $location, Notaries, NotaryUses, Services, Serventias) {

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.notary = {};

		$scope.title = 'Novo cartório';
	    $scope.action = 'Cadastrar';

	    $scope.uses = Array();
	    $scope.services = Array();

    	Serventias.cartorio(null, function(notaryuses) {
    		$scope.notaryuses = notaryuses;
    		$scope.notary.notaryuses = [];
	    });

	    Services.getAll(null, function(data) {
	    	$scope.notaryservices = data;
	   	});

	   	$scope.addNotaryUse = function(notaryuse) {
	   		var data = $scope.notary.notaryuses;
	   		for (var i = 0; i < data.length; i++) {
	   			if(data[i].id_tipo_serventias == notaryuse.id_tipo_serventias){
	   				return;
	   			}
	   		};

    		$scope.notary.notaryuses.push(notaryuse);
	    }

	    $scope.removeNotaryUse = function(index) {
	    	$scope.notary.notaryuses.splice(index, 1);
	    }

	   	$scope.addNotaryService = function(notaryservice) {
    		var index = $scope.services.indexOf(notaryservice);

	    	if(index == -1) {
	    		$scope.services.push(notaryservice);
	    	}
	    }

	    $scope.removeNotaryService = function(notaryservice) {
	    	var index = $scope.services.indexOf(notaryservice);
	    	$scope.services.splice(index, 1);
	    }

	    $scope.save = function(notary, notaryuses, notaryservices) {
	    	notary.serventias = notaryuses;
	    	notary.servicos = notaryservices;

	    	Notaries.create(notary, function(data) {
	    		$location.path('/notaries.html');
	    	});
	    }
	}]);

	MetronicApp.controller('NotaryUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'Notaries', 'NotaryUses', 'Services', 'Serventias', function($rootScope, $scope, $location, $stateParams, Notaries, NotaryUses, Services, Serventias) {

		$scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.title = 'Editar cartório';
	    $scope.action = 'Editar';

		Notaries.getById($stateParams.id, function(data) {
			$scope.uses = Array();
			$scope.services = Array();

			$scope.notary = data;

	    	Serventias.cartorio(null, function(notaryuses) {
	    		$scope.notaryuses = notaryuses;
	    		$scope.notary.notaryuses = [];
		    });

	    	Notaries.getUses(data.id_cartorio, function(notaryuses) {
	    		$scope.notary.notaryuses = notaryuses;
	    	});

	    	Services.getAll(null, function(notaryservices) {
	    		$scope.notaryservices = notaryservices;
		    });

	    	Notaries.getServices(data.id_cartorio, function(notaryservices) {
	    		$scope.services = notaryservices;
	    	});
	    });

	   	$scope.addNotaryUse = function(notaryuse) {
	   		var data = $scope.notary.notaryuses;
	   		for (var i = 0; i < data.length; i++) {
	   			if(data[i].id_tipo_serventias == notaryuse.id_tipo_serventias){
	   				return;
	   			}
	   		};

    		$scope.notary.notaryuses.push(notaryuse);
	    }

	    $scope.removeNotaryUse = function(index) {
	    	$scope.notary.notaryuses.splice(index, 1);
	    }

	   	$scope.addNotaryService = function(notaryservice) {
    		var index = $scope.services.indexOf(notaryservice);

	    	if(index == -1) {
	    		$scope.services.push(notaryservice);
	    	}
	    }

	    $scope.removeNotaryService = function(notaryservice) {
	    	var index = $scope.services.indexOf(notaryservice);
	    	$scope.services.splice(index, 1);
	    }

	    $scope.save = function(notary, notaryuses, notaryservices) {
	    	notary.serventias = notaryuses;
	    	notary.servicos = notaryservices;

	    	Notaries.update(notary.id_cartorio, notary, function(data) {
	    		$location.path('/notaries.html');
	    	});
	    }
	}]);

}());
