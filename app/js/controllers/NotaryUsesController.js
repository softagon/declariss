(function(){

	'use strict';

	MetronicApp.controller('NotaryUsesList', ['$rootScope', '$scope', '$location', 'NotaryUses', function($rootScope, $scope, $location, NotaryUses) {

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    NotaryUses.getAll(null, function(data){
	    	$scope.notaryuses = data;
	   	});

	    $scope.save = function(notaryuse){
	    	NotaryUses.create(notaryuse, function(data){
	    		$location.path('/notaryuses.html');
	    	});
	    }

	    $scope.modal = function(notaryuse_modal) {
	    	$scope.notaryuse_modal = notaryuse_modal;
	    }

	    $scope.remove = function(notaryuse) {
	    	NotaryUses.delete(notaryuse.id_serventia, function() {
	    		var index = $scope.notaryuses.indexOf(notaryuse);
	    		$scope.notaryuses.splice(index, 1);
	    	});
	    }
	}]);

	MetronicApp.controller('NotaryUseCreate', ['$rootScope', '$scope', '$location', 'NotaryUses', '$http', 'Serventias', function($rootScope, $scope, $location, NotaryUses, $http, Serventias) {

	    $scope.$on('$viewContentLoaded', function() { 
	    	Metronic.initAjax();
	    });

	    Serventias.tipoServentias(null , function(data){
	    	$scope.tipo_serventia = data[0];
	    	$scope.tipo_serventias = data;

	    	$scope.cartorioServentia(data[0].id_cartorio);
	    });

	    $scope.cartorioServentia = function(id){
	    	Serventias.All(id, function(serventia){
				$scope.cartorioServentias = serventia;
	    		$scope.cartorio_serventia = serventia[0];
	    		
	    		if(serventia[0]){
	    			$scope.getBydata(serventia[0].id_tipo_serventias);
	    		}else{
	    			$scope.serventias = {};
	    		}
	    	});
	    }
		
		$scope.getBydata = function(id){
		    Serventias.getAll(id, function(data){
		    	$scope.serventias = $scope.alterarValor(data);
		    });
		}

	    $scope.alterarValor = function(data){
	    	$scope.valor = { bruto: 0, issqn: 0, n_de_atos: 0 };

	    	for (var i = data.length - 1; i >= 0; i--) {

	    		if(data[i].n_de_atos && data[i].valor_emolumento){
	    			data[i].bruto = data[i].n_de_atos * data[i].valor_emolumento;
	    			data[i].issqn = 5 * data[i].bruto / 100;

	    			$scope.valor.issqn += data[i].issqn; 
	    			$scope.valor.bruto += data[i].bruto;
					$scope.valor.n_de_atos += parseInt(data[i].n_de_atos);

	    			data[i].valor_emolumento = parseInt(data[i].valor_emolumento); 
	    		}else{
	    			data[i].n_de_atos = "";
	    			data[i].bruto = 0;
	    			data[i].issqn = 0;
	    		}
	    	};

	    	return data;
	    };

	    $scope.changeAtos = function(index, tipo){;
	    	var data = $scope.serventias;

			data[index].n_de_atos = parseInt(data[index].n_de_atos);

	    	if(tipo == "add"){
	    		data[index].n_de_atos += 1;
	    	}else{
	    		if(data[index].n_de_atos > 0){
	    			data[index].n_de_atos -= 1;
	    		}
	    	}

	    	$scope.serventias = $scope.alterarValor(data);
	    }

	    $scope.saveNotary = function(data){
	    	$scope.load = true;
	    	Serventias.update(null, data, function(data){
	    		$scope.load = false;
	    	});
	    };

		$scope.title = 'Nova serventia';
	    $scope.action = 'Cadastrar';

	    $scope.save = function(notaryuse) {
	    	NotaryUses.create(notaryuse, function(data) {
	    		$location.path('/notaryuses.html');
	    	});
	    }
	}]);

	MetronicApp.controller('NotaryUseUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'NotaryUses', function($rootScope, $scope, $location, $stateParams, NotaryUses) {

		$scope.$on('$viewContentLoaded', function() {
			Metronic.initAjax();
	    });

		NotaryUses.getById($stateParams.id, function(data) {
			$scope.notaryuse = data;
	    });

	    $scope.title = 'Editar serventia';
		$scope.action = 'Editar';

	    $scope.save = function(notaryuse) {
	    	NotaryUses.update(notaryuse.id_serventia, notaryuse, function(data) {
	    		$location.path('/notaryuses.html');
	    	});
	    }
	}]);

}());
