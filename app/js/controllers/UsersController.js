(function(){   

	'use strict';

	MetronicApp.controller('UsersList', ['$rootScope', '$scope', '$location', 'Users', function($rootScope, $scope, $location, Users){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Users.getAll(null, function(data){
	    	$scope.users = data;
	    });

	    $scope.save = function(user){
	    	Users.create(user, function(data){
	    		$location.path('/users.html');
	    	})
	    }

	    $scope.modal = function(user) {
	    	$scope.userm = user;
	    }

	    $scope.remove = function(user) {
	    	Users.delete(user.id_usuario, function(){
	    		var index = $scope.users.indexOf(user);
				$scope.users.splice(index, 1);
	    	})
	    }


	}]);

	MetronicApp.controller('UserCreate', ['$rootScope', '$scope', '$location', 'Users', 'Townhalls', function($rootScope, $scope, $location, Users, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.townchecks = Array();

    	Townhalls.getAll(null, function(towns){
	    	$scope.townhalls = towns;
	    });

	    Users.roles(null, function(data){
	    	$scope.roles = data;
	    });

    	$scope.addItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	console.log(index);
	    	if(index == -1){
	    		$scope.townchecks.push(JSON.parse(townhall));
	    	}
	    }

	    $scope.removeItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	$scope.townchecks.splice(index, 1);
	    }

	    $scope.save = function(user, townhalls){
	    	user.prefeituras = townhalls;
	    	Users.create(user, function(data){
	    		$location.path('/users.html');
	    	})
	    }

	}]);

	MetronicApp.controller('UserUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'Users', 'Townhalls','$http' ,function($rootScope, $scope, $location, $stateParams, Users, Townhalls, $http){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Users.roles(null, function(data){
	    	$scope.roles = data;
	    });

	    $scope.townchecks = [];
		Users.getById($stateParams.id, function(data){

			$scope.townchecks = Array();
			delete data.senha;
			$scope.user = data;

	    	Townhalls.getAll(null, function(towns){
		    	$scope.townhalls = towns;
		    });

	    	Users.getTownhalls(data.id_usuario, function(towns){
	    		$scope.townchecks = towns;
	    	});

	    	for (var i = 0; i < $scope.roles.length; i++) {

	    		if($scope.roles[i].id_roles == data.roles[0]['id_roles']){
	    			data.roles = $scope.roles[i];
	    			return;
	    		}
	    	};
	    });
		
		$scope.addItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	console.log(index);
	    	if(index == -1){
	    		$scope.townchecks.push(JSON.parse(townhall));
	    	}
	    }

	    $scope.removeItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	$scope.townchecks.splice(index, 1);
	    }

	    $scope.save = function(user, townhalls){
	    	user.prefeituras = townhalls;
	    	Users.update(user.id_usuario, user, function(data){
	    		$location.path('/users.html');
	    	})
	    }

	}]);

}());
