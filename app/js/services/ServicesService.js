(function(){    

    'use strict';

    MetronicApp.factory('Services', ['$http', 'Utils', function($http, Utils) {
        
        var service = {};
        var api = '../api/services';

        service.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        service.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        service.create = function(data, callback) {
        	return $http.post(api, data).success(callback);
        }

        service.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        service.delete = function(id, callback) {
        	return $http.delete(api + '/' + id).success(callback);
        }


        return service;

    }]);

}());