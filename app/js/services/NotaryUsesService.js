(function(){    

    'use strict';

    MetronicApp.factory('NotaryUses', ['$http', 'Utils', function($http, Utils) {
        
        var notaryuse = {};
        var api = '../api/notaryuses';

        notaryuse.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        notaryuse.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        notaryuse.create = function(data, callback) {
        	return $http.post(api, data).success(callback);
        }

        notaryuse.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        notaryuse.delete = function(id, callback) {
            return $http.delete(api + '/' + id).success(callback);
        }

        return notaryuse;
    }]);

    MetronicApp.factory('Serventias', ['$http', 'Utils', function($http, Utils) {
    
    var notaryuse = {};
    var api = '../api/serventias';

    notaryuse.getAll =  function(id, callback) {
        return $http.get(api + '/' + id).success(callback);
    }

    notaryuse.getById =  function(id, callback) {
        return $http.get(api + '/' + id).success(callback);
    }

    notaryuse.cartorio = function(id, callback) {
        return $http.get(api + '/tipos/cartorio').success(callback);
    }

    notaryuse.update = function(id, data, callback) {
        return $http.put(api + '/' + id, data).success(callback);
    }

    notaryuse.tipoServentias = function(id, callback) {
        return $http.get(api + '/tipos/all').success(callback);
    }

    notaryuse.All = function(id, callback) {
        return $http.get(api + '/tipos/'+ id).success(callback);
    }

    return notaryuse;
}]);
}());
