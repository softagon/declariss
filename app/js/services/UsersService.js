(function(){    

    'use strict';

    MetronicApp.factory('Users', ['$http', 'Utils', 'Prefeitura', function($http, Utils, Prefeitura) {
        
        var user = {};
        var api = '../api/users';

        user.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        user.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        user.getTownhalls =  function(id, callback) {
            return $http.get(api + '/townhalls/' + id).success(callback);
        }

        user.create = function(data, callback) {
        	return $http.post(api, data).success(callback);
        }

        user.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        user.delete = function(id, callback) {
        	return $http.delete(api + '/' + id).success(callback);
        }

        user.roles = function(id, callback){
            return $http.get(api + '/roles/all').success(callback);
        }


        return user;

    }]);

}());