<?php

class Service extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'servico';
    protected $primaryKey = 'id_servico';
    public $timestamps = false;

}