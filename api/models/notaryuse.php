<?php

class NotaryUse extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'cartorio_serventia';
	protected $primaryKey = 'id_serventia';
    public $timestamps = false;
}

class Serventias extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'serventias';
	protected $primaryKey = 'id_serventias';
    public $timestamps = false;
}

class TipoServentias extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'tipo_serventias';
	protected $primaryKey = 'id_tipo_serventias';
    public $timestamps = false;
}

