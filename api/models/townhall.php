<?php

class TownHall extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'prefeitura';
    protected $primaryKey = 'id_prefeitura';
    public $timestamps = false;

    public function cartorio(){
        return $this->hasMany ('TownHallAsCartorio', 'id_prefeitura');

    }

    public function documentos(){
        return $this->hasMany ('Documentos', 'id_prefeitura');

    }
}

class TownHallAsCartorio extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'prefeitura_has_cartorio';
    protected $primaryKey = 'id_prefeitura';
    public $timestamps = false;


}

class CartorioHasCartorio extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'cartorio_has_cartorio_serventia';
    protected $primaryKey = 'id_cartorio';
    public $timestamps = false;

}

class Documentos extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'documentos';
    protected $primaryKey = 'id_prefeitura';
    public $timestamps = false;

}