<?php

class User extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'usuario';
	protected $primaryKey = 'id_usuario';
	public $timestamps = false;

	public function roles(){
       return $this->hasMany ('RolesUser', 'id_usuario')
                    ->join('roles', 'roles.id_roles', '=', 'roles_user.id_roles');
    }
}

class UserHasTowHall extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'usuario_has_prefeitura';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;
}

class Roles extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'roles';
    protected $primaryKey = 'id_roles';
    public $timestamps = false;
}

class RolesUser extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'roles_user';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;
}
