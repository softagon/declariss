<?php

require_once 'controllers/townhalls.php';
require_once 'controllers/auth.php';

$app->get('/prefeitura', function () use ($app) {
	\Auth\isAuthenticate();
	\TownHalls\model($app);
});

//GET Route
$app->get('/townhalls', function () use ($app) {
	\Auth\isAuthenticate();
	\TownHalls\retrieve($app);
});

//GET Route
$app->get('/townhalls/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\TownHalls\retrieve($app, $id);
});

//POST Route
// $app->post('/townhalls', function () use ($app) {
// 	\Auth\isAuthenticate();
// 	\TownHalls\create($app);
// });

//PUT Route
$app->post('/townhalls/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\TownHalls\update($app, $id);
});

//DELETE Route
$app->delete('/townhalls/:id/', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\TownHalls\remove($app, $id);
});