<?php

require_once 'controllers/services.php';
require_once 'controllers/auth.php';

//GET Route
$app->get('/services', function () use ($app) {
	\Auth\isAuthenticate();
	\Services\retrieve($app);
});

//GET Route
$app->get('/services/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Services\retrieve($app, $id);
});

//POST Route
$app->post('/services', function () use ($app) {
	\Auth\isAuthenticate();
	\Services\create($app);
});

//PUT Route
$app->put('/services/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Services\update($app, $id);
});

//DELETE Route
$app->delete('/services/:id/', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Services\remove($app, $id);
});