<?php

require_once 'controllers/users.php';
require_once 'controllers/auth.php';

//GET Route
$app->get('/users', function () use ($app) {
	\Auth\isAuthenticate();
	\Users\retrieve($app);
});

//GET Route
$app->get('/users/townhalls/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Users\townhalls($app, $id);
});

//GET Route
$app->get('/users/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Users\retrieve($app, $id);
});

//POST Route
$app->post('/users', function () use ($app) {
	\Auth\isAuthenticate();
	\Users\create($app);
});

//PUT Route
$app->put('/users/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Users\update($app, $id);
});

//DELETE Route
$app->delete('/users/:id/', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Users\remove($app, $id);
});

$app->get('/users/roles/all', function () use ($app) {
	\Auth\isAuthenticate();
	\Users\roles($app);
});