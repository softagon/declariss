(function () {

    'use strict';

    MetronicApp.factory('Prefeitura', [ function() {
    var Prefeitura;
    Prefeitura = (function() {
      function Prefeitura(data) {
        this.setData(data);
      }

      Prefeitura.prototype.setData = function(data) {
    	  angular.extend(this, data);
      };
      
      Prefeitura.prototype.getId = function() {
    	  return this.id;
      };

      return Prefeitura;
    })();
    return new Prefeitura(<?= json_encode($prefeitura); ?>);	
}]);

}());