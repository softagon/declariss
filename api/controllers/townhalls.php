<?php

namespace TownHalls;

use Respect\Validation\Validator as v;

require_once APP . '/models/townhall.php';
require_once APP . '/models/user.php';

//Retrieve
function retrieve($app, $id = 0){

	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		$townhalls = \TownHall::with('cartorio')->with('documentos')->find($id);
	else
		$townhalls = \TownHall::take($limit)->offset($offset)->orderBy('id_prefeitura', 'desc')->get();

    if($townhalls)
    	\utils::json($app, $townhalls->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);

}

//Update
function update($app, $id=0){
	$body = json_encode($_POST);	
	$body = json_decode($body);

	if($id){
		$townhall = \TownHall::find($id);
	}else{
		$townhall = new \TownHall;
	}

    if(isset($_FILES['administracao']))
        $administracao = $_FILES['administracao'];

    if(isset($_FILES['municipal']))
        $municipal = $_FILES['municipal'];


	if(isset($administracao) || isset($municipal)){

		$file_nome = date('Y-m-d H:i');
		$random = md5($file_nome);
		
		if(isset($municipal)){
			$image_name = $municipal['name'];
			$extension_imagem = substr($image_name,strrpos($image_name,'.')+0);
			$file_municipal = $random . $extension_imagem;

			if(isset($townhall->municipal) AND empty(!$townhall->municipal)){
				unlink('../app/upload/imagem/' . $townhall->municipal);	
			}
			move_uploaded_file($municipal['tmp_name'], '../app/upload/imagem/' . $file_municipal);
		}

		if(isset($administracao)){
			$image_name = $administracao['name'];
			$extension_imagem = substr($image_name,strrpos($image_name,'.')+0);
			$file_administracao = md5($random) . $extension_imagem;
			if(isset($townhall->administracao) AND empty($townhall->administracao)){
				unlink('../app/upload/imagem/' . $townhall->administracao);	
			}
			move_uploaded_file($administracao['tmp_name'], '../app/upload/imagem/' . $file_administracao);
		}
	}

	if(is_object($body)) {

		$validator = v::attribute('nome', v::string()->length(5,45))
					->attribute('cidade', v::string()->length(5,45))
					->attribute('uf', v::string()->length(5,45));

		if($validator->validate($body)) {
			$townhall->cnpj = $body->cnpj;
			$townhall->nome = $body->nome;
	    	$townhall->cidade = $body->cidade;
	    	$townhall->uf = $body->uf;
	    	$townhall->documento = $body->documento;
	    	$townhall->prazo = $body->prazo;
	    	$townhall->dispositivo = $body->dispositivo;
	    	$townhall->codigo = $body->codigo;

			if(isset($municipal)){
				$townhall->municipal = $file_municipal;
			}
			if(isset($administracao)){
	    		$townhall->administracao = $file_administracao;
	    	}
			$result = $townhall->save();

			$documentos = json_decode($body->documentos);
			
			$dol = \Documentos::find($townhall->id_prefeitura);
			if($dol)
				$dol->delete();

			if(isset($documentos)){
				foreach ($documentos as $key => $value) {
					$documentos = new \Documentos();
					$documentos->nome = $value->nome;
					$documentos->dispositivo = $value->dispositivo;
					$documentos->prazo = $value->prazo;
					$documentos->id_prefeitura = $townhall->id_prefeitura;
					$documentos->save();
				}
			}

			$catorio = \TownHallAsCartorio::find($townhall->id_prefeitura);
			if($catorio){
				$catorio->delete();
			}

			if($result) {
				$body->cartorios = json_decode($body->cartorios);
	
				if(!empty($body->cartorios)){
					foreach ($body->cartorios as $key => $value) {
						$catorio = new \TownHallAsCartorio();
						$catorio->id_prefeitura = $townhall->id_prefeitura;
						$catorio->id_cartorio = $value->id_cartorio;
						$catorio->save();
					}
				}

				\utils::json($app, $townhall->toJson());
				return;
			}

		}
	}

}

//Remove
function remove($app, $id){

	$townhalls = \UserHasTowHall::where('id_prefeitura', '=', $id)->get();

	if($townhalls->count() == 0){
		$townhall = \TownHall::find($id);

		if($townhall) {

            if(isset($townhall->municipal)){
                $filename = '../app/upload/imagem/'. $townhall->municipal;
                if (file_exists($filename)) {
                    unlink($filename);
                }
            }

            if(isset($townhall->administracao)){
                $filename = '../app/upload/imagem/'. $townhall->administracao;
                if (file_exists($filename)) {
                    unlink($filename);
                }
            }

			$townhall->delete();
			\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
		} else {
			\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
		}
	} else {
		$app->response->setStatus(400);
	}

}

//Model
function model($app){
	if(!isset($_SESSION)){
		session_start();
	}

	if(array_key_exists('user', $_SESSION)){
		$user = $_SESSION['user'];
		$user = unserialize($user);

		$prefeitura = \RolesUser::join('roles', 'roles.id_roles', '=', 'roles_user.id_roles')
			->find($user['id_usuario']);
		if($prefeitura){
			$app->render('prefeitura/model.js', array('prefeitura' => $prefeitura));			
			return;
		}
	}
}