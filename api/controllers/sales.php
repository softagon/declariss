<?php

namespace Sales;

use Respect\Validation\Validator as v;

require_once APP . '/models/sale.php';

//Retrieve
function retrieve($app, $id = 0){

	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		//$sales = \Sale::find($id);
		$sales = \Sale::where('venda.id_venda', '=', $id)
	->join('servico', 'servico.id_servico', '=', 'venda.id_servico')->first();
	else
		$sales = \Sale::join('servico', 'servico.id_servico', '=', 'venda.id_servico')->take($limit)->offset($offset)->orderBy('id_venda', 'desc')->get();

	if($sales)
    	\utils::json($app, $sales->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);

}

//Create
function create($app){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_array($body)) {

		foreach($body as $key => $val) {

			$saleValidation = v::attribute('quantidade', v::int())
							->attribute('id_servico', v::int());

			if($saleValidation->validate($val)) {

				$sale = new \Sale();
				$sale->quantidade = $val->quantidade;
				if(isset($val->numero_pedido))
					$sale->numero_pedido = $val->numero_pedido;
		    	$sale->id_servico = $val->id_servico;
				$id = $sale->save();

				if(!$id) {
					$app->response->setStatus(400);
				}

			}

		}

		\utils::json($app, array('message' => 'All data inserted!'), true);
		return;

	}
	
	$app->response->setStatus(400);

}

//Remove
function remove($app, $id){

	$sale = \Sale::find($id);
	if($sale) {
		$sale->delete();
		\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
	} else {
		\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
	}

}