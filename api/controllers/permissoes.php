<?php

namespace Permissoes;

function auth($permissoes=null){
	if(!isset($_SESSION)){
		session_start();
	}

	if(array_key_exists('user', $_SESSION)){

		$user = $_SESSION['user'];
		$user = unserialize($user);

		//Pegando dados do acesso
		$user = \User::find($user['id_usuario']);

		$roles = \RolesUser::join('roles', 'roles.id_roles', '=', 'roles_user.id_roles')
					->find($user->id_usuario);

		if($roles){
			if(isset($permissoes)){
				foreach ($permissoes as $permissao) {
					if($permissao == $roles->name)
						return;
				}
			}else{
				return;
			}
		}
			
	}

	die('Not Authenticated!');
}