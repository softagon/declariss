<?php

namespace NotaryUses;

use Respect\Validation\Validator as v;

require_once APP . '/models/townhall.php';
require_once APP . '/models/notaryuse.php';
require_once APP . '/models/user.php';

// retrieve

function retrieve($app, $id = 0) {
	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		$uses = \NotaryUse::find($id);
	else
		$uses = \NotaryUse::take($limit)->offset($offset)->orderBy('id_serventia', 'desc')->get();

    if($uses)
    	\utils::json($app, $uses->toJson());
    else
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
}

// create

function create($app) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {
		$validation = v::attribute('nome', v::string()->length(1,45));

		if($validation->validate($body)) {
			$notaryUse = new \NotaryUse();
			$notaryUse->nome = $body->nome;

			$id = $notaryUse->save();

			if($id) {
				\utils::json($app, $notaryUse->toJson());
				return;
			}
		}
	}

	$app->response->setStatus(400);
}

// update

function update($app, $id) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {
		$validation = v::attribute('nome', v::string()->length(1,45));

		if($validation->validate($body)) {
			$notaryUse = \NotaryUse::find($id);
			$notaryUse->nome = $body->nome;

			$id = $notaryUse->save();

			if($id) {
				\utils::json($app, $notaryUse->toJson());
				return;
			}
		}
	}

	$app->response->setStatus(400);
}

// remove

function remove($app, $id) {
	$notaryUse = \NotaryUse::find($id);

	if($notaryUse) {
		$notaryUse->delete();
		\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
	} else {
		\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
	}
}

function Serventias($app, $id=0) {
	if($id)
		$uses = \Serventias::where('id_tipo_serventias_FK', '=', $id)->get();

    if($uses){
		\utils::json($app, $uses->toJson());
    }
}

function updateServentias($app) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	foreach ($body as $key => $value) {
		$serventias = \Serventias::find($value->id_serventias);
		$serventias->valor_emolumento = $value->valor_emolumento;
		$serventias->n_de_atos = $value->n_de_atos;
		$serventias->save();
	}
	return;
}

function tipoServentia($app) {

	if(array_key_exists('user', $_SESSION)){
		$user = $_SESSION['user'];
		$user = unserialize($user);
	}

	$prefeitura = \UserHasTowHall::find($user['id_usuario']);
	

	$cartorios = \TownHallAsCartorio::where('prefeitura_has_cartorio.id_prefeitura', '=', $prefeitura->id_prefeitura)
		->join('cartorio', 'cartorio.id_cartorio', '=', 'prefeitura_has_cartorio.id_cartorio')
		->join('prefeitura', 'prefeitura.id_prefeitura', '=', 'prefeitura_has_cartorio.id_prefeitura')
		->get(['prefeitura.*', 'cartorio.*', 'cartorio.nome as nome', 'prefeitura.nome as prefeitura']);

	foreach ($cartorios as $key => $value) {
		$value->user = $user['nome'];
	}

	if($cartorios)
		\utils::json($app, $cartorios->toJson());
}

function ServentiasAll($app) {

	$serventias = \TipoServentias::all();

	if($serventias)
		\utils::json($app, $serventias->toJson());
}

function serventiaCartorio($app, $id) {
	$cartorioServentia = \CartorioHasCartorio::where('id_cartorio', '=', $id)->get();
	$result = array();

	foreach ($cartorioServentia as $key => $value) {
		if($cartorioServentia){
			$serventias = $tipo_serventias = \TipoServentias::find($value->id_tipo_serventias);

			if($serventias)
				array_push($result, $serventias);
		}
	}


	echo json_encode($result);
	return;
}