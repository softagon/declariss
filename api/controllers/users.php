<?php

namespace Users;

use Respect\Validation\Validator as v;

require_once APP . '/models/user.php';

//Retrieve
function retrieve($app, $id = 0){

	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id){
		$users = \User::with('roles')->find($id);
	}
	else{

		if(!isset($_SESSION)){
			session_start();
		}

		if(array_key_exists('user', $_SESSION)){

		$user = $_SESSION['user'];
		$user = unserialize($user);
		$prefeitura = \UserHasTowHall::find($user['id_usuario']);

		if($prefeitura){
		$users = \UserHasTowHall::join('usuario', 'usuario.id_usuario', '=', 'usuario_has_prefeitura.id_usuario')
								->join('roles_user', 'roles_user.id_usuario', '=', 'usuario.id_usuario')
								->join('roles', 'roles.id_roles', '=', 'roles_user.id_roles')
								->where('id_prefeitura', '=', $prefeitura->id_prefeitura)->get();
		}
		}
	}
	if($users)
    	\utils::json($app, $users->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);

}

//Create
function create($app){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$userValidator = v::attribute('nome', v::string()->length(5,45))
						->attribute('email', v::email())
						->attribute('senha', v::string()->length(6,45));

		if($userValidator->validate($body)) {

			$user = new \User();
			$user->nome = $body->nome;
	    	$user->email = $body->email;
	    	$user->senha = @crypt($body->senha);
			$user->ip = $_SERVER['REMOTE_ADDR'];
			$user->navegador = $_SERVER['HTTP_USER_AGENT'];
			$result = $user->save();


			$roleUser = \RolesUser::find($user->id_usuario);
			if($roleUser)
				$roleUser->delete();

			$roleUser = new \RolesUser();
			$roleUser->id_roles = $body->roles->id_roles;
			$roleUser->id_usuario = $user->id_usuario;
			$roleUser->save();

			if($result) {

				$townhalls = $body->prefeituras;
				foreach ($townhalls as $key => $val) {
					
					$townhall = new \UserHasTowHall();
					$townhall->id_prefeitura = $val->id_prefeitura;
					$townhall->id_usuario = $user->id_usuario;
					$townhall->save();
				}

				\utils::json($app, $user->toJson());
				return;
			}

		}

	}
	
	$app->response->setStatus(400);

}

//Update
function update($app, $id){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$userValidator = v::attribute('nome', v::string()->length(5,45))
						->attribute('email', v::email());

		if($userValidator->validate($body)) {

			$user = \User::find($id);
			$user->nome = $body->nome;
	    	$user->email = $body->email;
	    	if(isset($body->senha))
	    		$user->senha = @crypt($body->senha);
			$user->ip = $_SERVER['REMOTE_ADDR'];
			$user->navegador = $_SERVER['HTTP_USER_AGENT'];
			$result = $user->save();

			$roleUser = \RolesUser::find($user->id_usuario);
			if($roleUser)
				$roleUser->delete();

			$roleUser = new \RolesUser();
			$roleUser->id_roles = $body->roles->id_roles;
			$roleUser->id_usuario = $user->id_usuario;
			$roleUser->save();

			if($result) {

				\UserHasTowHall::where('id_usuario', '=', $id)->delete();
				$townhalls = $body->prefeituras;

				foreach ($townhalls as $key => $val) {
					
					$townhall = new \UserHasTowHall();
					$townhall->id_prefeitura = $val->id_prefeitura;
					$townhall->id_usuario = $user->id_usuario;
					$townhall->save();
				}

				\utils::json($app, $user->toJson());
				return;
			}

		}

	}
	
	$app->response->setStatus(400);

}

//Remove
function remove($app, $id){

	$user = \User::find($id);

	$roleUser = \RolesUser::find($id);
	if($roleUser)
		$roleUser->delete();

	if($user) {
		$user->delete();
		\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
	} else {
		\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
	}

}

//Retrieve
function townhalls($app, $id){

	$townhalls = \UserHasTowHall::where('id_usuario', '=', $id)
	->join('prefeitura', 'prefeitura.id_prefeitura', '=', 'usuario_has_prefeitura.id_prefeitura')->get();

	if($townhalls)
    	\utils::json($app, $townhalls->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);

}

//Update
function roles($app){
	$roles = \Roles::all();

	if($roles)
    	\utils::json($app, $roles->toJson());
}