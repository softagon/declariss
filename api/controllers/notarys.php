<?php

namespace Notarys;

use Respect\Validation\Validator as v;

require_once APP . '/models/notary.php';

// retrieve

function retrieve($app, $id = 0) {
	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		$notarys = \Notary::find($id);
	else
		$notarys = \Notary::take($limit)->offset($offset)->orderBy('id_cartorio', 'desc')->get();

	if($notarys)
		\utils::json($app, $notarys->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
}

// create

function create($app) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$validation = v::attribute('nome', v::string()->length(1, 45))
						->attribute('documento', v::string()->length(1, 45))
						->attribute('endereco', v::string()->length(5, 45))
						->attribute('cidade', v::string()->length(3, 45))
						->attribute('uf', v::string()->length(3, 45))
						->attribute('tabeliao', v::string()->length(6, 45))
						->attribute('cep', v::string()->length(9, 9))
						->attribute('responsavel', v::string()->length(3, 45));

		if($validation->validate($body)) {
			$notary = new \Notary();

			$notary->nome = $body->nome;
			$notary->documento = $body->documento;
			$notary->endereco = $body->endereco;
	    	$notary->cidade = $body->cidade;
	    	$notary->uf = $body->uf;
	    	$notary->tabeliao = $body->tabeliao;
	    	$notary->cep = $body->cep;
	    	$notary->responsavel = $body->responsavel;

	    	$result = $notary->save();

			if($result) {
				// insert on cartorio_has_cartorio_serventia
				$notaryuses = $body->notaryuses;

				foreach ($notaryuses as $key => $val) {
					$notaryuse = new \NotaryHasNotaryUse();
					$notaryuse->id_tipo_serventias = $val->id_tipo_serventias;
					$notaryuse->id_cartorio = $notary->id_cartorio;

					$notaryuse->save();
				}

				// insert on cartorio_has_servico
				$notaryservices = $body->servicos;

				foreach ($notaryservices as $key => $val) {
					$notaryservice = new \NotaryHasService();
					$notaryservice->id_servico = $val->id_servico;
					$notaryservice->id_cartorio = $notary->id_cartorio;

					$notaryservice->save();
				}

				\utils::json($app, $notary->toJson());
				return;
			}
		}
	}

	$app->response->setStatus(400);
}

// update

function update($app, $id) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {
		$validation = v::attribute('nome', v::string()->length(1, 45))
						->attribute('documento', v::string()->length(1, 45))
						->attribute('endereco', v::string()->length(5, 45))
						->attribute('cidade', v::string()->length(3, 45))
						->attribute('uf', v::string()->length(3, 45))
						->attribute('tabeliao', v::string()->length(6, 45))
						->attribute('cep', v::string()->length(9, 9))
						->attribute('responsavel', v::string()->length(3, 45));

		if($validation->validate($body)) {
			$notary = \Notary::find($id);

			$notary->nome = $body->nome;
			$notary->documento = $body->documento;
			$notary->endereco = $body->endereco;
	    	$notary->cidade = $body->cidade;
	    	$notary->uf = $body->uf;
	    	$notary->tabeliao = $body->tabeliao;
	    	$notary->cep = $body->cep;
	    	$notary->responsavel = $body->responsavel;

	    	$result = $notary->save();

	    	if($result) {
	    		// cartorio_has_cartorio_serventia
				\NotaryHasNotaryUse::where('id_cartorio', '=', $id)->delete();
				$notaryuses = $body->notaryuses;

				foreach ($notaryuses as $key => $val) {
					$notaryuse = new \NotaryHasNotaryUse();
					$notaryuse->id_tipo_serventias = $val->id_tipo_serventias;
					$notaryuse->id_cartorio = $notary->id_cartorio;

					$notaryuse->save();
				}

				// cartorio_has_servico
				\NotaryHasService::where('id_cartorio', '=', $id)->delete();
				$notaryservices = $body->servicos;

				foreach ($notaryservices as $key => $val) {
					$notaryservice = new \NotaryHasService();
					$notaryservice->id_servico = $val->id_servico;
					$notaryservice->id_cartorio = $notary->id_cartorio;

					$notaryservice->save();
				}

				\utils::json($app, $notary->toJson());
				return;
			}
		}
	}

	$app->response->setStatus(400);
}

// remove

function remove($app, $id) {

	$notary = \Notary::find($id);

	$notarys = \NotaryHasNotaryUse::where('id_cartorio', '=', $id)->get();

	if($notarys->count() == 0){

		if($notary) {
			$notary->delete();
			\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
		} else {
			\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
		}

	}else {
		$app->response->setStatus(400);
	}

}

// retrieve notaryuses

function notaryuses($app, $id) {
	$notaryuses = \NotaryHasNotaryUse::where('id_cartorio', '=', $id)
	->join('tipo_serventias', 'tipo_serventias.id_tipo_serventias', '=', 'cartorio_has_cartorio_serventia.id_tipo_serventias')->get();

	\utils::debug($notaryuses->toJson());

	if($notaryuses)
		\utils::json($app, $notaryuses->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
}

// retrieve notaryservices

function notaryservices($app, $id) {
	$notaryservices = \NotaryHasService::where('id_cartorio', '=', $id)
	->join('servico', 'servico.id_servico', '=', 'cartorio_has_servico.id_servico')->get();

	\utils::debug($notaryservices->toJson());

	if($notaryservices)
		\utils::json($app, $notaryservices->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
}
