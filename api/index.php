<?php

//APP Constant
define('APP', dirname(__FILE__));

//Composer autoload
require APP . '/vendor/autoload.php';

//Slim register
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$app = new \Slim\Slim(array(
  'debug' => true,
  'templates.path' => APP . '/view/'
));

//Some requires
require APP . '/config/database.php';
require APP . '/config/routes.php';
require APP . '/helpers/utils.php';
require APP . '/controllers/permissoes.php';

//require USERS routes
require APP . '/routes/users.php';

//require TOWNHALL routes
require APP . '/routes/townhalls.php';

//require NOTARYS routes
require APP . '/routes/notarys.php';

//require NOTARYUSES routes
require APP . '/routes/notaryuses.php';

//require SERVICES routes
require APP . '/routes/services.php';

//require SALES routes
require APP . '/routes/sales.php';

//require AUTH routes
require APP . '/routes/auth.php';

$app->run();
