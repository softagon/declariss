<?php

// Database settings
$settings = array(
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'database' => 'declariss',
    'username' => 'root',
    'password' => 'root',
    'charset'   => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => ''
);

use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule;
$capsule->addConnection( $settings );
$capsule->bootEloquent();